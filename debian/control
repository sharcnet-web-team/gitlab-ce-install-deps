Source: gitlab-ce
Section: misc
Priority: optional
Maintainer: Servilio Afre Puentes <afrepues@sharcnet.ca>
Build-Depends: debhelper (>= 9), libparse-debianchangelog-perl
Standards-Version: 3.9.5
Homepage: https://gitlab.com/sharcnet-web-team/gitlab-ce-install-deps
Vcs-Browser: https://gitlab.com/sharcnet-web-team/gitlab-ce-install-deps
Vcs-Git: git@gitlab.com:sharcnet-web-team/gitlab-ce-install-deps.git

Package: gitlab-ce
Depends: git (>= 1.7.10),
	 ruby2.1 | ruby (>= 2.1), ruby2.1-dev | ruby-dev (>= 2.1),
	 ruby-rails (>= 2:4.1), ruby-rails (<< 2:4.2),
	 ruby-rails-autolink (>= 1.1), ruby-rails-autolink (<< 2.0),
	 gitlab-ce-mysql (= ${source:Version}) | gitlab-ce-pgsql (= ${source:Version}),
	 ruby-omniauth (>= 1.1.3), ruby-omniauth (<< 1.2),
	 ruby-omniauth-google-oauth2, ruby-omniauth-twitter, ruby-omniauth-github,
	 ruby-grack (>= 2), ruby-grack (<< 2.1),
	 ruby-omniauth-ldap (>= 1.0.4), ruby-omniauth-ldap (<< 1.1),
	 ruby-rack-cors,
	 ruby-stamp,
	 ruby-kaminari (>= 0.15.1), ruby-kaminari (<< 0.16),
	 ruby-haml-rails,
	 ruby-six,
	 ruby-github-markup, ruby-redcarpet (>= 2.2.2), ruby-redcarpet (<< 2.3),
	 ruby-redcloth, ruby-org, ruby-creole (>= 0.3.6), ruby-creole (<< 0.4),
	 ruby-wikicloth (>= 0.8.1), ruby-wikicloth (<< 0.8.2),
	 asciidoctor (>= 0.1.4), asciidoctor (<< 0.2),
	 ruby-diffy (>= 3.0.3), ruby-diffy (<< 3.1),
	 unicorn (>= 4.6.3), unicorn (<< 4.7),
	 ruby-state-machine,
	 ruby-acts-as-taggable-on,
	 ruby-slim, ruby-sinatra, ruby-sidekiq (>= 2.17), ruby-sidekiq (<< 2.17.1),
	 ruby-httparty,
	 ruby-colored,
	 ruby-settingslogic,
	 ruby-tinder (>= 1.9.2), ruby-tinder (<< 1.10),
	 ruby-sanitize (>= 2), ruby-sanitize (<< 3),
	 ruby-sass-rails (>= 4.0.2), ruby-sass-rails (<< 4.1),
	 ruby-coffee-rails,
	 ruby-uglifier,
	 ruby-turbolinks,
	 ruby-jquery-rails, ruby-jquery-ui-rails,
	 ruby-font-awesome-rails (>= 3.2), ruby-font-awesome-rails (<< 4),
	 ruby-gon (>= 5), ruby-gon (<< 5.1),
	 ruby-request-store,
	 ruby-virtus,
	 openssh-server,
	 redis-server, logrotate, mail-transport-agent,
	 libicu-dev, libpq-dev, libxslt1-dev,
	 cmake, pkg-config
Recommends: nginx
Suggests: python-docutils, perl-modules (>= 5.10) | libpod-simple-perl, asciidoc
Architecture: all
Description: Open source software to collaborate on code
 Gitlab is an open source software that offers integrated Git
 repository management, code reviews, issue tracking, activity feeds,
 wikis and more.

Package: gitlab-ce-mysql
Depends: ruby-mysql2
Recommends: gitlab-ce (= ${source:Version}),
	    mysql-client | virtual-mysql-client
Suggests: mysql-server
Architecture: all
Description: metapackage providing MySQL dependencies for Gitlab
 This package only provides MySQL dependencies for Gitlab, an open
 source software that offers integrated Git repository management,
 code reviews, issue tracking, activity feeds, wikis and more.
 .
 Install this metapackage if you want to use a MySQL database with
 Gitlab.

Package: gitlab-ce-pgsql
Depends: ruby-pg
Recommends: gitlab-ce (= ${source:Version}),
	    postgresql-client
Suggests: postgresql
Architecture: all
Description: metapackage providing PostgreSQL dependencies for Gitlab
 This package only provides PostgreSQL dependencies for Gitlab, an
 open source software that offers integrated Git repository
 management, code reviews, issue tracking, activity feeds, wikis and
 more.
 .
 Install this metapackage if you want to use a PostgreSQL database
 with Gitlab.

Package: gitlab-ce-aws
Depends: ruby-fog (>= 1.14), ruby-fog (<< 2), ruby-unf
Recommends: gitlab-ce (= ${source:Version})
Architecture: all
Description: metapackage providing AWS dependencies for Gitlab
 This package only provides AWS dependencies for Gitlab, an open
 source software that offers integrated Git repository management,
 code reviews, issue tracking, activity feeds, wikis and more.
 .
 Install this metapackage if you want to use a AWS storage with
 Gitlab.

Package: gitlab-ce-dev
Depends: ruby-sdoc, thin, ruby-coveralls, ruby-rspec-rails,
	 ruby-capybara (>= 2.2.1), ruby-capybara (<< 2.3),
	 pry, ruby-awesome-print,
	 ruby-database-cleaner,
	 ruby-factory-girl-rails,
	 ruby-minitest (>= 5.3), ruby-minitest (<< 5.4),
	 ruby-rb-inotify,
	 ruby-spring (= 1.1.1),
	 ruby-simplecov,
	 ruby-shoulda-matchers (>= 2.1), ruby-shoulda-matchers (<< 2.2),
	 ruby-webmock,
	 ruby-test-after-commit
Recommends: gitlab-ce (= ${source:Version})
Architecture: all
Description: metapackage providing development dependencies for Gitlab
 This package only provides development dependencies for Gitlab, an
 open source software that offers integrated Git repository
 management, code reviews, issue tracking, activity feeds, wikis and
 more.
 .
 Install this metapackage if you want work on developing Gitlab, it
 includes dependencies from the «development» and «test» groups.
